import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {StyleGuideComponent} from './style-guide/style-guide.component';
import {HelloPageComponent} from './hello-page/hello-page.component';

const appRoutes: Routes = [
  {path: 'style-guide', component: StyleGuideComponent},
  {path: 'hello-page', component: HelloPageComponent},
  {path: '', redirectTo: '/heroes', pathMatch: 'full'},
  {path: '**', component: StyleGuideComponent} // <-- TODO 404 Page not found component?
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes,
      {enableTracing: true} // <-- true - debugging only
    )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
}
