import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {PageHeaderComponent} from './page-header.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {LoggerService} from '../../core/services/logger.service';
import {RouterTestingModule} from '@angular/router/testing';

describe('PageHeaderComponent', () => {
    let component: PageHeaderComponent;
    let fixture: ComponentFixture<PageHeaderComponent>;
    const fakeRoutes = [{path: 'user', redirectTo: '', pathMatch: 'full'}];

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                NgbModule.forRoot(),
                RouterTestingModule.withRoutes(fakeRoutes)
            ],
            declarations: [
                PageHeaderComponent
            ],
            providers: [
                LoggerService
            ]
        });
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PageHeaderComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
