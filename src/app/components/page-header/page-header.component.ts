import { Component, OnInit } from '@angular/core';
import {LoggerService} from '../../core/services/logger.service';

@Component({
  selector: 'app-page-header',
  templateUrl: './page-header.component.html',
  styleUrls: ['./page-header.component.scss']
})
export class PageHeaderComponent implements OnInit {

  routes = [
    {menuName: 'Hello', path: '/hello-page'},
    {menuName: 'Heroes', path: '/heroes'},
    {menuName: 'App Style Guide', path: '/style-guide'},
  ];

  isCollapsed = true;

  constructor(public logger: LoggerService) { }

  ngOnInit() {
      this.logger.log('PageHeaderComponent loaded');
  }

  toggleNavigationCollapse() {
    this.isCollapsed = !this.isCollapsed;
  }

  collapseNavigation() {
    this.isCollapsed = true;
  }

}
