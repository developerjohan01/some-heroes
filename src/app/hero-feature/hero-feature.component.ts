import { Component, OnInit } from '@angular/core';
import {LoggerService} from '../core/services/logger.service';

@Component({
  selector: 'app-hero-feature',
  templateUrl: './hero-feature.component.html',
  styleUrls: ['./hero-feature.component.scss']
})
export class HeroFeatureComponent implements OnInit {

  constructor(public logger: LoggerService) { }

  ngOnInit() {
      this.logger.log('HeroFeatureComponent loaded');
  }

}
