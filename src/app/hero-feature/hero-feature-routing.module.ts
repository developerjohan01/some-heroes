import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HeroFeatureComponent} from './hero-feature.component';

const heroRoutes: Routes = [
    {path: 'heroes', component: HeroFeatureComponent},
];

@NgModule({
    imports: [RouterModule.forChild(heroRoutes)],
    exports: [RouterModule]
})
export class HeroFeatureRoutingModule {
}
