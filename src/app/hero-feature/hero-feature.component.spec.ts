import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {HeroFeatureComponent} from './hero-feature.component';
import {LoggerService} from '../core/services/logger.service';

describe('HeroFeatureComponent', () => {
    let component: HeroFeatureComponent;
    let fixture: ComponentFixture<HeroFeatureComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                HeroFeatureComponent
            ],
            providers: [
                LoggerService
            ]
        });
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(HeroFeatureComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
