import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {HeroFeatureRoutingModule} from './hero-feature-routing.module';
import {HeroFeatureComponent} from './hero-feature.component';

@NgModule({
    imports: [
        CommonModule,
        HeroFeatureRoutingModule
    ],
    declarations: [
        HeroFeatureComponent
    ]
})
export class HeroFeatureModule {
}
