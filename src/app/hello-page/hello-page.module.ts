import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CoreModule} from '../core/core.module';
import {HelloPageComponent} from './hello-page.component';

@NgModule({
    imports: [
        CommonModule,
        CoreModule
    ],
    declarations: [
        HelloPageComponent
    ],
    exports: [
        HelloPageComponent
    ]
})
export class HelloPageModule {
}
