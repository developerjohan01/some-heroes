import {Component, OnInit} from '@angular/core';
import {LoggerService} from '../core/services/logger.service';

@Component({
    selector: 'app-hello-page',
    templateUrl: './hello-page.component.html',
    styleUrls: ['./hello-page.component.scss']
})
export class HelloPageComponent implements OnInit {

    title = 'Angular';

    constructor(public logger: LoggerService) {
    }

    ngOnInit() {
        this.logger.log('HelloPageComponent loaded');
    }

}
