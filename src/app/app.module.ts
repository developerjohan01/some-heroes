import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {StyleGuideComponent} from './style-guide/style-guide.component';
import {AppRoutingModule} from './app-routing.module';
import {ReactiveFormsModule} from '@angular/forms';
import {CoreModule} from './core/core.module';
import {SharedModule} from './shared/shared.module';
import {PageHeaderComponent} from './components/page-header/page-header.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {HelloPageModule} from './hello-page/hello-page.module';
import { HeroFeatureComponent } from './hero-feature/hero-feature.component';
import {HeroFeatureModule} from './hero-feature/hero-feature.module';

@NgModule({
    declarations: [
        AppComponent,
        StyleGuideComponent,
        PageHeaderComponent
    ],
    imports: [
        HelloPageModule,
        HeroFeatureModule,
        BrowserModule,
        ReactiveFormsModule,
        CoreModule,
        SharedModule,
        NgbModule.forRoot(),
        AppRoutingModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
