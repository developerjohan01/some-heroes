import {async, TestBed} from '@angular/core/testing';
import {AppComponent} from './app.component';
import {PageHeaderComponent} from './components/page-header/page-header.component';
import {AppRoutingModule} from './app-routing.module';
import {ReactiveFormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {StyleGuideComponent} from './style-guide/style-guide.component';
import {HelloPageComponent} from './hello-page/hello-page.component';
import {APP_BASE_HREF} from '@angular/common';
import {CoreModule} from './core/core.module';

describe('AppComponent', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                AppRoutingModule,
                ReactiveFormsModule,
                CoreModule,
                NgbModule.forRoot()
            ],
            declarations: [
                AppComponent,
                StyleGuideComponent,
                HelloPageComponent,
                PageHeaderComponent
            ],
            providers: [{provide: APP_BASE_HREF, useValue: '/'}]
        });
    }));
    it('should create the app', async(() => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    }));
    it('should contain app-page-header tag', async(() => {
        const fixture = TestBed.createComponent(AppComponent);
        fixture.detectChanges();
        const compiled = fixture.debugElement.nativeElement;
        expect(compiled.querySelector('app-page-header')).toBeTruthy();
    }));
});
