import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-style-guide',
  templateUrl: './style-guide.component.html',
  styleUrls: ['./style-guide.component.scss']
})
export class StyleGuideComponent implements OnInit {

  title = 'Style Guide';
  styleGuideMenuForm: FormGroup;
  searchField: FormControl;
  exampleInputEmail1: FormControl;
  styleGuideForm: FormGroup;

  constructor() { }

  ngOnInit() {
    console.log('Init style-guide');
    this.searchField = new FormControl('');
    this.styleGuideMenuForm = new FormGroup({
      searchField: this.searchField
    });
    this.exampleInputEmail1 = new FormControl('bob@fred.com');
    this.styleGuideForm = new FormGroup({
      exampleInputEmail1: this.exampleInputEmail1
    });

  }

}
